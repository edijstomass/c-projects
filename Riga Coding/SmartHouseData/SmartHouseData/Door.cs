﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartHouseData
{
   public class Door : ClosableLockableBase   // kols nozime, ka door bus visas tas pasas istabas, kas noraditaja klase
    {
        public Door(DoorGoupEnum doorGoup, DoorTypeEnum type)
        {
            Group = doorGoup;
            Type = type;
        }

        public DoorGoupEnum Group { get; private set; }

        public DoorTypeEnum Type { get; private set; }

    }
}
