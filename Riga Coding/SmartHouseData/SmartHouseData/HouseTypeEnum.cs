﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartHouseData
{
    public enum HouseTypeEnum
    {
        AppartmentHouse,
        Cottage,
        Bungalow,
        Castle
    }
}
