﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartHouseData
{
    public class Adress
    {
       
        public Adress(string streetName, string number, string postalIndex, string city, string flatNumber)
        {
            StreetName = streetName;
            Number = number;
            PostalIndex = postalIndex;
            City = city;
            FlatNumber = flatNumber;
        }
        public Adress(string houseName, string postalIndex, string city, string flatNumber = "")
        {
            HouseName = houseName;
            PostalIndex = postalIndex;
            City = city;
            FlatNumber = flatNumber;
        }


        public string StreetName { get; private set; }   //var tikai nolasiit
        public string Number { get; private set; }
        public string FlatNumber { get; private set; }
        public string HouseName { get; private set; }
        public string PostalIndex { get; private set; }
        public string City { get; private set; }


        // te bus publiskas metodes// kkas ar mantosanu saistits

        public override string ToString()   // ka tas izskatisies, ja kkur konvertes uz stringu
        {
            var streetOrHouseName = "";
            if(!string.IsNullOrEmpty(StreetName))
            {
                streetOrHouseName = $"{StreetName} {Number}";
                streetOrHouseName = HouseName;
            }
            else
            {
                streetOrHouseName = HouseName;
            }

            // var izmantot:  var streetOrHouseName = true ? n1 : n2



            return $"City : {City}; Postal Index : {PostalIndex}; Street/Name: {streetOrHouseName}";
        }

    }
}
