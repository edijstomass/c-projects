﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartHouseData
{
    public class House     // Centralais objekts// klases nosaukumam jasakrit ar faila nosaukumu
    {
        public House(Adress adress, double size, HouseTypeEnum type)
        {
            Adress = adress;
            Size = size;
            Type = type;
        }



        // publiskas ipasibas raksta ar lielo burtu! Adress is publiska ipasiba

        public Adress Adress { get; private set; }  // gan nolasit , gan piekskjirt // get = izsaucot, set pieskjirot
        public double Size { get; private set; }
        public HouseTypeEnum Type { get; private set; }
        public bool IsLocked { get; private set; } // so stavokli var samainit tikai saja klasee
        public List<Door> Doors { get; private set; } = new List<Door>();
        public List<Window> Windows { get; private set; } = new List<Window>();



        public override string ToString() // sis attiecas tikai uz House
        {
            return $"Adress: {Adress} Size: {Size} Type: {Type}";
        }


        //Metodes par durvim ( lock vai unlock)



        public bool Lock()    //metode aizsledz logus un durvis
        {
            IsLocked = true;

            foreach(var window in Windows)
            {
                window.Lock();
            }

            var outsideDoors = Doors.Where(d => d.Group == DoorGoupEnum.Outside);
                foreach (var door in outsideDoors)
            {
                door.Lock();
            }

            return true;
        }




        public bool Unlock()  // Atsledz durvis
        {
            IsLocked = false;

            var frontDoors = Doors.Where(d => d.Type == DoorTypeEnum.Front);
            foreach(var frontDoor in frontDoors)
            {
                frontDoor.Unlock();
            }

            return true;
        }


    }
}
