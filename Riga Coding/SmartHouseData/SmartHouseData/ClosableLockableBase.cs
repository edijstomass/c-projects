﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartHouseData
{
   public abstract class ClosableLockableBase    // abstact - sis klases objektu nevar uztaisit pa taisno // mantosanas metode
    {
        protected ClosableLockableBase()  
        {
            IsLocked = false;  // uzreiz nav aizslegts vai aizverts
            IsClosed = false;
        }
        
        
        public bool IsLocked { get; private set; } 
        public bool IsClosed { get; private set; }

        public bool Lock()
        {
            IsLocked = true;
            return true;
        }

        public bool Unlock()
        {
            IsClosed = false;
            return true;
        }

        public bool Open()
        {
            IsClosed = false;
            return true;
        }

        public bool Close()
        {
            IsClosed = true;
            return true;
        }




    }
}
