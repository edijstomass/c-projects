﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartHouseData
{
    public enum DoorTypeEnum
    {
        Front,
        Back,
        Garage,
        Living
    }
}
