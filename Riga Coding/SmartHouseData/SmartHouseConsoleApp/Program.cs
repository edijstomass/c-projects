﻿using SmartHouseData;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SmartHouseConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // House ir musu izveidots Data Tips

            Adress adress1 = new Adress("Berzi", "LV1010", "Riga", "5A");


            House house1 = new House(adress1, 34, HouseTypeEnum.Cottage);
            House house2 = new House(new Adress("Brivibas", "23", "LV1010", "Riga", "211"), 56, HouseTypeEnum.Castle);

            //var adress = house1.Adress;
            //house1 = "asdasdasd";

            List<House> houseList = new List<House>();
            houseList.Add(house1);
            houseList.Add(house2);

            //Console.WriteLine(house1.GetFullInfo());
            //Console.WriteLine(house2.GetFullInfo());


           var housesWith2 = houseList.Where(h => !string.IsNullOrEmpty(h.Adress.StreetName) && h.Adress.StreetName.Contains("B"));

            foreach (var house in houseList)
            {
                Console.WriteLine(house);

            }



        }
    }
}
