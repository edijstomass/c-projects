﻿using SmartHouseData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SmartHouseWpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private House house1;
        private Adress adress1;

        

        public MainWindow()
        {
            //House house1 = new House(new Adress("Berzini", "LV1010", "Riga", "5"), 55, HouseTypeEnum.AppartmentHouse);
            //  ja liek garo, tad augsa nevajag dekleret private House un Adress

            adress1 = new Adress("Berzini", "LV1010", "Riga", "5");
            house1 = new House(adress1, 55, HouseTypeEnum.Castle);

            InitializeComponent();

        }

        private void Lock_Click(object sender, RoutedEventArgs e)
        {
            house1.Lock();
            HouseStatus.Text = house1.IsLocked.ToString();
            House23.Fill = Brushes.Red;
        }

        private void Unlock_Click(object sender, RoutedEventArgs e)
        {
            house1.Unlock();
            HouseStatus.Text = house1.IsLocked.ToString();
            House23.Fill = Brushes.Green;
        }
    }
}
