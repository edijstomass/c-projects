﻿using System;


namespace Task_16_Find_In_array_name
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] names = new string[] { "Edijs", "Janis", "Zane", "Justine", "Gusts" };

            Console.WriteLine("Which name position do you want to find?");

            var findName = Array.IndexOf(names, Console.ReadLine());

            Console.WriteLine(findName);

            // Vai sadi : 

            //Console.WriteLine(Array.IndexOf(names, "Justine"));

        }
    }
}
