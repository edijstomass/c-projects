﻿using System;

namespace IncapsulationSample_OOP1
{
    class Program
    {
        static void Main(string[] args)
        {

            var door1 = new Door(100, 50);   // jauna objekta instance
            var door2 = new Door(210, 0);

            //door1.Height = 12;  


            // Pec private un konstruktora izveidas, paramtri jauzliek uzreiz
            // Var tikai nolasiit

            Console.WriteLine(door1.Width);

            //private tikai klase
            //protected - var ari, ja kada klase mantojas

            Console.WriteLine(door1.Validate());
            Console.WriteLine(door2.Validate());



        }
    }
}
