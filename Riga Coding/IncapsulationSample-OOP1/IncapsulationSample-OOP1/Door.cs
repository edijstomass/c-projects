﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IncapsulationSample_OOP1
{
    public class Door
    {

        public Door(double height, double width)
        {
            Height = height;
            Width = width;
        }

        public double Height { get; private set; }
        public double Width { get; private set; }




        // Pub metode 
        public bool Validate()
        {
            
            return ValidateHeight() && ValidateWidth();  // Inline code, parbauda vai abi ir True.. Ja abi ir True, tad return bus true
        }


        private bool ValidateWidth()
        {
            return Width > 0;   // true vai false... Ja bus lielaks, tad return atgriezis True
        }


        private bool ValidateHeight()
        {
            return Height > 0;
        }
    }
}
