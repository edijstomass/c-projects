﻿using System;

namespace Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintName();
            PrintName("Teksts");
            Console.WriteLine(CalcSum(2));
            CalcSum(x: 4);
        }


        static void PrintName()
        {
            Console.WriteLine("Gusts");
        }



        static void PrintName(string name)
        {
            Console.WriteLine(name);
        }



        static int CalcSum(int x, int y =5)
        {
            return x + y;
        }



    }
}
