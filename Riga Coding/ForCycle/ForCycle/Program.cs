﻿using System;

namespace ForCycle
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new[] { 1, 24 , 3, 4, -5, 8, 0, 23 };
            string[] names = new[] { "Gusts", "Janis" };
            int i;

            //for (i = 0; i<numbers.Length; i++)
            //{
            //    //Console.WriteLine($"{i} - {numbers[i]}");
            //    if (numbers[i] > 5)
            //    {
            //        Console.WriteLine(numbers[i]);
            //    }
            //}


            //for (i = 0; i < names.Length; i++)
            //{



            //    if (names[i].Substring(0,1) == "J" )
            //    {
            //        Console.WriteLine(names[i]);
            //    }

            //}

            int BiggestElement = int.MinValue;


            for(i=0; i<numbers.Length; i++)
            {

                if(numbers[i] > BiggestElement)
                {
                    BiggestElement = numbers[i];
                }


            }
            Console.WriteLine($"The Biggest element is: {BiggestElement}");


            int lowestElement = int.MaxValue;

            foreach(int number in numbers)
            {
                if(number < lowestElement)
                {
                    lowestElement = number;

                }


            }

            Console.WriteLine($"The Biggest element is: {lowestElement}");




        }
    }
}
