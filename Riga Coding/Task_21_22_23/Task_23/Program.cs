﻿using System;

namespace Task_23
{
    class Program
    {
        static void Main(string[] args)
        {

            int[,] twoDimension = new int[5, 5];


            int count = 1;
            int diagonalSum = 0;


            for (int i = 0; i < twoDimension.GetLength(0); i++)
            {

                for (int j = 0; j < twoDimension.GetLength(1); j++)
                {
                    twoDimension[i, j] = count;
                    count++;
                    Console.Write(twoDimension[i, j] + "\t");
                    if(i == j)
                    {
                        diagonalSum += twoDimension[i, j];
                    }
                }
                Console.WriteLine();
            }

            Console.WriteLine();
            int total = 0;
            foreach(int number in twoDimension)
            {
                total += number;
            }

            Console.WriteLine($"Total element sum is: {total} ");
            Console.WriteLine($"Diagonal sum is: {diagonalSum}");

        }
    }
}
