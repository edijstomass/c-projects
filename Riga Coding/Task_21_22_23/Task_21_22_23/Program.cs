﻿using System;
using System.Linq;

namespace Task_21_22_23
{
    class Program
    {
        static void Main(string[] args)
        {
            
            int[] numbers = new int[] { 1, 56, 77, 89, 34, 22, 2, 3, 4, 8, 43, 56 };


            Console.WriteLine("Numbers 0 - 30");
            for (int i = 0; i < numbers.Length; i++)
            {

                if (numbers[i] >= 0 && numbers[i] <= 30)
                {
                    Console.WriteLine(numbers[i]);
                }
            }

            Console.WriteLine("Numbers 31 - 60");
            for (int i = 0; i < numbers.Length; i++)
            {

                if (numbers[i] > 30 && numbers[i] <= 60)
                {
                    Console.WriteLine(numbers[i]);
                }
            }

            Console.WriteLine("Numbers 61 - 90");
            for (int i = 0; i < numbers.Length; i++)
            {

                if (numbers[i] > 60 && numbers[i] <= 90)
                {
                    Console.WriteLine(numbers[i]);
                }
            }


            //// IZMANTOJOT LINQ


            //var condition1 = numbers.Where(number => number >= 0 && number <= 30);
            //var condition2 = numbers.Where(number => number > 30 && number <= 60);
            //var condition3 = numbers.Where(number => number > 60 && number <= 90);






        }
    }
}
