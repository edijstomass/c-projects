﻿using System;

namespace WhileCycle
{
    class Program
    {
        static void Main(string[] args)
        {
            // while // labs cikla organizesanas mehanisms, daris

            int[] numbers = new[] { 1, 3, 4, 8, 0, 23 };
            string[] names = new[] { "Gusts", "Janis" };

            int i = 0;
            

            while (i < numbers.Length)
            {
                Console.WriteLine(numbers[i]);
                i++;
            }

            i = 0;

            while (i < names.Length)
            {
                Console.WriteLine(names[i]);
                i++;
            }




        }
    }
}
