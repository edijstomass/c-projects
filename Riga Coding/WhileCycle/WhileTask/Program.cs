﻿using System;

namespace WhileTask
{
    class Program
    {
        static void Main(string[] args)
        {
            //bool userInputIsInvalid = true;
            //string userInput;
            //int number = 0;


            //while (userInputIsInvalid)  // dari, kammer user input is invalid
            //{

            //    Console.Write("Enter number: ");
            //    userInput = Console.ReadLine();

            //    try
            //    {
            //        number = Convert.ToInt32(userInput);
            //        userInputIsInvalid = false;
            //    }
            //    catch (Exception)
            //    {
            //        Console.WriteLine("Please enter valid number");
            //    }

            //}

            //Console.WriteLine($"You entered {number}");

            int number = 0;

            while(true)
            {
                Console.Write("Enter any number: ");
                string userInput = Console.ReadLine();

                try
                {
                    number = Convert.ToInt32(userInput);
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("Enter valid number!");
                }

            }

            Console.WriteLine($"You entered: {number}");











        }
    }
}
