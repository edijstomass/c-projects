﻿using System;
using System.Collections.Generic;

namespace Interface2
{
    class Program
    {
        static void Main(string[] args)
        {

            IPolicy policy = new CarPolicy();
            IPolicy travelPolicy = new TravelPolicy();

            List<IPolicy> policies = new List<IPolicy>();
            policies.Add(policy);
            policies.Add(travelPolicy);

            foreach (var item in policies)
            {
                item.Print();
            }

        }
    }
}
