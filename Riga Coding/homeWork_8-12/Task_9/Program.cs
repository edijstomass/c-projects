﻿using System;

namespace Task_9
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[] { 100, 200, 233, 66, 77, 98 };

            Console.WriteLine($"First value: {numbers.GetLowerBound(0)} and last value: {numbers.GetUpperBound(0)}");

            Console.WriteLine($"First element: {numbers[0]} and last element: {numbers[5]}");

        }
    }
}
