﻿using System;

namespace Task_12
{
    class Program
    {
        enum Day { Monday = 1, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday };

        static void Main(string[] args)
        {

            Day forTask = Day.Friday;


            var firstDay = Day.Monday;      // var == Day
            int secondDay = (int)Day.Tuesday;  //shows value


            Console.WriteLine($"{forTask} \n{firstDay} \n{secondDay}");


        }
    }
}
