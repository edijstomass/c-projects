﻿using System;

namespace Task_11
{
    class Program
    {
        static void Main(string[] args)
        {

            string[] names = { "Jānis", "Pēteris", "Andris" };

            Console.WriteLine(names[1]+ "\n");

            names[2] = "Jēkabs";

            Console.WriteLine("New Values :\n");
            for(int i = 0; i<names.Length; i++)
            {
                Console.WriteLine(names[i]);
            }


        }
    }
}
