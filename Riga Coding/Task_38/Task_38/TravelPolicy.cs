﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_38
{
   public class TravelPolicy : PolicyBase
    {
        public int TravelDays { get; set; }
        public int TravelContinent { get; set; }



        public override double CalculatePremium()
        {
            return TravelDays + TravelContinent;
        }







    }

}
