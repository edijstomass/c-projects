﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_38
{
   public class KaskoPolicy : PolicyBase
    {

        public int CarAge { get; set; }
        public int AccidentCount { get; set; }


        public override double CalculatePremium()
        {
            return CarAge + AccidentCount;
        }

    }
}
