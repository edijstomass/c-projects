﻿using System;

namespace IfElseAndSwitch
{
    class Program
    {
        enum Answer
        {
            Yes, // vertiba 0... atbildei var iedot citu vertibu, piem Yes=1; No=2;
            No, // vertiba 1
            None  // u.t.t.
        }

        static void Main(string[] args)
        {
            Answer answer1 = Answer.Yes;
            

            string name = "Gusts";
            string firstChar = name.Substring(0, 1);
            
            
            //if (firstChar == "A")
            //{
            //    Console.WriteLine("A");
            //}
            //else if(firstChar == "B")
            //{
            //    Console.WriteLine("B");
            //}
            //else if(firstChar == "C")
            //{
            //    Console.WriteLine("C");
            //}
            //else
            //{
            //    Console.WriteLine("Others");
            //}

            switch (firstChar)
            {
                case "A":
                    Console.WriteLine("A");
                    break;
                case "B":
                    Console.WriteLine("B");
                    break;
                case "C":
                    Console.WriteLine("C");
                    break;
                default:
                    Console.WriteLine("Others");
                    break;
            }


            switch (answer1)
            {
                case Answer.Yes:
                    Console.WriteLine("A");
                    break;
                case Answer.No:
                    Console.WriteLine("B");
                    break;
                case Answer.None:
                    Console.WriteLine("C");
                    break;
                default:
                    Console.WriteLine("Others");
                    break;
            }
            Console.ReadLine();





        }
    }
}
