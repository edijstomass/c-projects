﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NameEntryApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<string> nameList = new List<string>();
        List<TodoItem> items = new List<TodoItem>();
        public MainWindow()
        {
            InitializeComponent();
            
            items.Add(new TodoItem() { Title = "Complete this WPF tutorial", Completion = 45 });
            items.Add(new TodoItem() { Title = "Learn C#", Completion = 80 });
            items.Add(new TodoItem() { Title = "Wash the car", Completion = 10 });

            icTodoList.ItemsSource = items;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(txtTitle.Text) && !string.IsNullOrEmpty(txtCompletion.Text))
            {
                var newItem = new TodoItem();
                newItem.Title = txtTitle.Text;
                newItem.Completion = int.Parse(txtCompletion.Text);

                items.Add(newItem);
                icTodoList.ItemsSource = null;
                icTodoList.ItemsSource = items;
            
            }
        }












    }
    public class TodoItem
    {
        public string Title { get; set; }
        public int Completion { get; set; }
    }
}
