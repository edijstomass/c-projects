﻿using System;

namespace BreakAndContenue
{
    class Program
    {
        static void Main(string[] args)
        {

            //string[] names = new[] { "Gusts", "Janis", "Peteris" };

            //foreach(var name in names)
            //{
            //    if(name == "Janis")
            //    {
            //        Console.WriteLine(name);
            //        break;                   //tad kad atradis janis, programma tuprinasies aiz foreach
            //    }


            //}

            //Console.WriteLine("All elements checked");


            string[] names = new[] { "Gusts", "Janis", "Peteris","Jans" };

            foreach (var name in names)
            {
                if (name == "Janis")
                {
                    Console.WriteLine("Lecam pie nakosa elementa");
                    continue;   // ja iedarbojas continue, 
                                //tad foreach turpina no augsas (izlaiz nakamas darbibas saja loop reize)
                                //  neizdruka Janis un sak baudit nakamo name
                }

                Console.WriteLine(name); 
            }

            Console.WriteLine("All elements checked");





        }
    }
}
