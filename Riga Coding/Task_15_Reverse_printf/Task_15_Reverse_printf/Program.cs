﻿using System;
using System.Linq;

namespace Task_15_Reverse_printf
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] numbers = new int[] { 5, 7, 8, 9, 3, 5, 1, 7, 8, 0 };


            foreach (var number in numbers.Reverse())
            {
                Console.WriteLine(number);
            }

            // VAI arii 2 rindinjas ar linq

            //var new_var = numbers.Reverse();
            //foreach(var number in new_var)
            //{
            //    Console.WriteLine(number);
            //}


            Console.WriteLine("Bez LinQ");

            int n = numbers.Length-1;

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[n]);
                --n;
            }








        }
    }
}
