﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inheritence
{
   public abstract class Shape
    {

        private int _colorCode = 1;

        //sadi izskatas sakumaa

        //public virtual double GetArea()   // virtual - katra klase, kas mantojas no shape, varam uzrakstit savu aprekinu, ka izrekinat laukumu
        //{                               
        //    return 0;
        //}

        // virtual lauj izmantot override



        // virtual - katra klase, kas mantojas no shape, varam uzrakstit savu aprekinu, ka izrekinat laukumu

            //metode
        public abstract double GetArea();   // janmaina klase uz apstract



        public int GetColorCode()
        {
            return _colorCode;
        }


    }
}
