﻿using System;
using System.Collections.Generic;

namespace Inheritence
{
    class Program
    {
        static void Main(string[] args)
        {
            var rec1 = new Rectangle();
            rec1.GetArea();    // GetArea metode mantojaas no citas klases
            rec1.Height = 12;
            rec1.Width = 34;

            Console.WriteLine(rec1.GetArea());
            rec1.GetColorCode();



            var circle1 = new Circle()
            {
                Radius = 5
            };
           

            Console.WriteLine(circle1.GetArea());

            circle1.GetColorCode();



            //new Shape(); Par cik Shape klase tagad ir abstract, nevar vairs uztaisit jaunu objektu


            List<Shape> shapes = new List<Shape>();

            shapes.Add(rec1);
            shapes.Add(circle1);

            foreach(var shape in shapes)
            {
                if(shape is Rectangle)
                {
                    var rec3 = shape as Rectangle; // ja nenosprags, ta rec3 bus null
                    var rec4 = (Rectangle)shape; //casting // saja gadijuma, ja neizdosies, programma nomirs
                    if(rec3 != null)
                    {
                        Console.WriteLine(rec3.Width);
                    }
                }

                Console.WriteLine(shape.GetArea());
            }

            

        }
    }
}
