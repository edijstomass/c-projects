﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Inheritence
{
    class Circle : Shape
    {
        public double Radius { get; set; }


        public override double GetArea()
        {
            return Math.PI * (Radius * Radius);
        }


    }
}
