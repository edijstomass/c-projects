﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PasswordValidatorLibrary
{
   public class PassworValidator
    {
      
        const int MinPasswordLenght = 8;
        static List<string> ErroList = new List<string>();


        static void ValidatePasswordMatch(string password1, string password2)
        {
            // 1. Chech if password match
            if (password1 != password2)
            {
                ErroList.Add("Password do not match");
            }
        }
        
        

        static void ValidatePasswordLenght(string Passwordlenght)
        {
            // 2. Check password lenght
            if (Passwordlenght.Length < MinPasswordLenght)
            {
                ErroList.Add("Password is too short");
            }
        }

        static void ValidatePasswordLettersAndNumbers(string password)
        {
            // 3. Check if numbers and characters exist
            int letterCount = 0;
            int numberCount = 0;

            foreach (var ch in password)
            {
                if (char.IsDigit(ch))
                {
                    numberCount++;
                }
                else if (char.IsLetter(ch))
                {
                    letterCount++;
                }
            }

            if (letterCount == 0)
            {
                ErroList.Add("Your password does not contain any letters");
            }

            if (numberCount == 0)
            {
                ErroList.Add("Your password does not contain any number");
            }


        }


        public static IEnumerable<string> ValidatePassword(string password, string password2)
        {
            ErroList.Clear();
            ValidatePasswordMatch(password, password2);
            ValidatePasswordLenght(password);
            ValidatePasswordLettersAndNumbers(password);

            return ErroList;
        }

    }
}
// IEnumberable nozime liste, kura ir read only,, galvenaja programma neko nevar pievienot