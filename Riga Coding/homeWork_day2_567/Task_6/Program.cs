﻿using System;

namespace Task_6
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Choose the object of house: ");
            string objectOfHouse = Console.ReadLine();
            string firstLetter = objectOfHouse.Substring(0, 1);
            firstLetter = firstLetter.ToLower();
            //firstLetter = Char.ToUpper(firstLetter[0]) + firstLetter.Substring(1);


            if (firstLetter == "a" || firstLetter == "b" ||
                firstLetter == "c" || firstLetter == "d")
            {
                Console.WriteLine("First floor");
            }
            else if (firstLetter == "e" || firstLetter == "f" ||
                     firstLetter == "g" || firstLetter == "h")
            {
                Console.WriteLine("Second floor");
            }
            else if (firstLetter == "i" || firstLetter == "j" ||
                     firstLetter == "k" || firstLetter == "l")
            {
                Console.WriteLine("Third floor");
            }
            else if (firstLetter == "m" || firstLetter == "n" ||
                     firstLetter == "o" || firstLetter == "p")
            {
                Console.WriteLine("Fourth floor");
            }
            else if (firstLetter == "r" || firstLetter == "s" ||
                     firstLetter == "t" || firstLetter == "u")
            {
                Console.WriteLine("fifth floor");
            }
            else
            {
                Console.WriteLine("To the basement");
            }


        }
    }
}
