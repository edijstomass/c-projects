﻿using System;

namespace Methods2
{
    class Program
    {
        static void Main(string[] args)
        {
            // PrintName("Edijs");

            //string name = "Test Name";
            //string surname = "Testu Surname";

            // PrintNameSurname(name, surname);
            // Console.WriteLine($"{name} {surname}");

            //var bday = new DateTime(1993, 1, 2);
            //netaa var atrast c# DataTime formatus

            //PrintBirthDay("Edijs", DateTime.Now);
            //Console.WriteLine();
            //PrintBirthDay("Edijs", bday);


            //PrintLotsOfData("Gusts", bday, "Zem tilta", 4, 5);
            //PrintLotsOfData("Gusts", bday, "Zem tilta");


            // PrintFullName("Edijs", "Tomass");


            // Convert Value
            int x;
            var result = int.TryParse("4", out x);  // ja izdevas convert 4 par ciparu, tad result atgriezis true; un x bus 4;

            if(!result) // ja result false
            {

            } else  // bet ja izdevas convertet
            {
                Console.WriteLine(x);
            }

            //Calculate car premuim

            double premium;
           var premiumResult = CalculatePremuim(3, 1, 1, out premium);

            if(premiumResult)
            {
                Console.WriteLine($"Your premium is {premium}");
            }else
            {
                Console.WriteLine("Unable to calculate premium");
            }



        }

        static void PrintText()
        {
            int x = 1;
            int y = 56;
            Console.WriteLine(x + y);
        }


        static void PrintName(string name)
        {
            Console.WriteLine(name);
        }


        static void PrintNameSurname(string name, string surname)
        {
            Console.WriteLine($"{name} {surname}");
            name = "Edijs";
            surname = "Tomass";

        }

        static void PrintBirthDay(string name, DateTime bdate)
        {
            Console.WriteLine($"{name} {bdate.ToShortDateString()}");
            Console.WriteLine($"{name} {bdate}");
        }

        static void PrintBirthDay(string name, DateTime bdate, string adress)
        {
            PrintBirthDay(name, bdate);  // Method Chaining
            Console.WriteLine(adress);
        }


        static void PrintLotsOfData(string name, DateTime bdate, string adress, int grade = 0, int avgGrade = 0)       //noklusetas parametru vertibas
        {

        }


        static void PrintFullName(string name, string surname = " ")
        {
            Console.WriteLine(name);
            //if (!string.IsNullOrEmpty(surname))
            //{
            //    Console.WriteLine(surname);
            //}

        }

        static void CalculateSum1(int x, int y)
        {
            if(x == 0 && y == 0)
            {
                return;
            }
            Console.WriteLine(x + y);

        }

        static int CalculateSum(int x, int y)
        {
            return x + y;
        }



        static double CalculateSum(double x, double y)
        {
            return x + y;
        }


        static bool CalculatePremuim(int carAge, int driveAge, int accidents, out double premium)  // out obligati metode japieskjir vertiba
        {

            if(carAge >= 0 && carAge <= 4)
            {
                premium = 30;
                return true;
            }
            else if(carAge > 15)
            {
                premium = -1;
                return false;
            }




            premium = -1;  // ja viss slikti, premium ir - 1
            return false;

        }





    }
}
