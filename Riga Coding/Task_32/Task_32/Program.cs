﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_32
{
    class Program
    {
        static void Main(string[] args)
        {

            Car car1 = new Car("BMW", "730M", 7350.50, "Black", 2007);
            Car car2 = new Car("Volvo", "S80", 3200, "Blue", 2003);
            Car car3 = new Car("Mercedes Benz", "E350", 6500, "White", 2008);
            Car car4 = new Car("Opel", "Vectra", 350, "Yellow", 1993);
            Car car5 = new Car("Audi", "A7", 68000, "Dark Gray", 2019);


            List<Car> carList = new List<Car>() { car1, car2, car3, car4, car5 };

 
            var findMostExpensiveCar = carList.Max(c => c.Price);
            var mostExpensiveCar = carList.Where(c => c.Price == findMostExpensiveCar);


            foreach (var car in mostExpensiveCar)
            {
                Console.WriteLine(car.FullCarInfo());
            }




        }
    }
}
