﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_32
{
    public class Car
    {

        public Car(string make, string model, double price, string color, int year)
        {
            Make = make;
            Model = model;
            Price = price;
            Color = color;
            Year = year;
        }



        public string Make { get; private set; }
        public string Model { get; private set; }
        public double Price { get; private set; }
        public string Color { get; private set; }
        public int Year { get; private set; }


        public string FullCarInfo()
        {
            return $"Car info: Make - {Make}, Model - {Model}, Price - {Price}, Color - {Color}, Year - {Year}";
        }


    }
}
