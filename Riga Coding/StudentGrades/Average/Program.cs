﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Average2
{

    class Program
    {
        static void my_method(string studentName, List<double> grades)
        {
            Console.WriteLine($"Average grades for {studentName} is {grades.Average()}");
        }

        static void Main(string[] args)
        {

            List<string> studentNames = new List<string>() {"Liene", "Lina", "Kristine" };

            List<double> studentGrades1 = new List<double>() { 8, 3, 5, 4, 2, 6, 8, 9, };
            List<double> studentGrades2 = new List<double>() { 8, 7, 5, 4, 10, 6, 8, 6, };
            List<double> studentGrades3 = new List<double>() { 8, 4, 5, 4, 2, 6, 5, 6, };

            my_method(studentNames[0], studentGrades1);
            my_method(studentNames[1], studentGrades2);
            my_method(studentNames[2], studentGrades3);



        }

    }
}
