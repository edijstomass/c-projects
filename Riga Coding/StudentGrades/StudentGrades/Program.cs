﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace StudentGrades
{
    class Program
    {
        static void PrintStudentGradeAvg(string student, List<int> grades)
        {
            Console.WriteLine($"Student name : {student} avg. grade {grades.Average()}");
        }

        static void Main(string[] args)

        {
            List<string> studentNames = new List<string>();
            studentNames.Add("Gusts");
            studentNames.Add("Peteris");
            studentNames.Add("Janis");

            List<int> student1Grades = new List<int>() {1, 5, 6, 7, 9, 3, 5, 1};

            List<int> student2Grades = new List<int>() { 5, 6, 3, 10, 1, 2, 4, 5, 5 };

            List<int> student3Grades = new List<int>() { 3, 4, 5, 9, 3, 5, 5, 4, };


            PrintStudentGradeAvg(studentNames[0], student1Grades);
            PrintStudentGradeAvg(studentNames[1], student2Grades);
            PrintStudentGradeAvg(studentNames[2], student3Grades);

        }



    }
}
