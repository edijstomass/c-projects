﻿using System;
using System.Collections.Generic;

namespace InterfaceUsage
{
    class Program
    {
        static void Main(string[] args)
        {
            List<ISomeSecondInterface> items = new List<ISomeSecondInterface>();
            items.Add(new Door());
            items.Add(new Window());

            foreach (var item in items)
            {
                // var sadi - item as Door
                item.GetColor();
            }

        }
    }
}
