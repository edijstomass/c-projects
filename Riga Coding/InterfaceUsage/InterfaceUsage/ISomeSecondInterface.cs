﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InterfaceUsage
{
   public interface ISomeSecondInterface
    {
        // pasakam, kadam metodem jaabut

        void GetColor();
        int GetColorCode();
    }
}
