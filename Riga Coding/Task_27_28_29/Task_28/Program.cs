﻿using System;

namespace Task_28
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[] { 5, 2, 2, 5, 10, 5, 55, 234 };
            string[] letters = new string[] { "Peteris", "Liga", "Zane", "Baiba", "Edijs" };

            PrintArrayValues(numbers);



        }



        static void PrintArrayValues(int[] numbers)
        {
            foreach(int number in numbers)
            {
                Console.WriteLine(number);
            }
        }


        static void PrintArrayValues(string[] letters)
        {
            foreach(string letter in letters)
            {
                Console.WriteLine(letter);
            }
        }




    }
}
