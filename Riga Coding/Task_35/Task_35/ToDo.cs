﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_35
{
   public class ToDo
    {

        public ToDo(string name, string descript, WeekDayEnum weekday, bool done)
        {
            Name = name;
            Description = descript;
            WeekDay = weekday;
            Done = done;

        }


        public string Name { get; private set; }
        public string Description { get; private set; }
        public WeekDayEnum WeekDay { get; private set; }
        public bool Done { get; private set; }



        public override string ToString()
        {
            return $"Task name: {Name}, Descript: {Description}, Day: {WeekDay}"; 
        }





    }
}
