﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_35
{
    class Program
    {
        static void Main(string[] args)
        {

            ToDo task1 = new ToDo("Work", "Complete homework", WeekDayEnum.Monday, false);
            ToDo task2 = new ToDo("Home", "Clean kitchen", WeekDayEnum.Friday, false);
            ToDo task3 = new ToDo("Home", "Wash dishes", WeekDayEnum.Wednesday, true);
            ToDo task4 = new ToDo("Home", "Buy cat food", WeekDayEnum.Friday, false);
            ToDo task5 = new ToDo("Home", "Pet my cat", WeekDayEnum.Sunday, true);
            ToDo task6 = new ToDo("Work", "Make a coffee", WeekDayEnum.Friday, false);
            ToDo task7 = new ToDo("Work", "Sleep all shift", WeekDayEnum.Thursday, false);


            List<ToDo> allTasks = new List<ToDo>();
            allTasks.Add(task1);
            allTasks.Add(task2);
            allTasks.Add(task3);
            allTasks.Add(task4);
            allTasks.Add(task5);
            allTasks.Add(task6);
            allTasks.Add(task7);

            var uncompletedTasks = allTasks.Where(d => d.Done == false);
            var tasksInFriday = allTasks.Where(d => d.WeekDay == WeekDayEnum.Friday && d.Done == false);

            Console.WriteLine("All uncompleted tasks: ");

            foreach (var item in uncompletedTasks)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
            Console.WriteLine("All uncompleted tasks in Friday: ");

            foreach (var item in tasksInFriday)
            {
                Console.WriteLine(item);
            }





        }
    }
}
