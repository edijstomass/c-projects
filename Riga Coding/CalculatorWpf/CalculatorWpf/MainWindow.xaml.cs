﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CalculatorWpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            txtNumber1.Background = Brushes.Green;
            txtNumber2.Background = Brushes.Green;
            double number1 = 0;
            double number2 = 0;
            try
            {
                number1 = Convert.ToDouble(txtNumber1.Text);  // ja viss labi
               // txtNumber1.Background = Brushes.Green;              // tad kraasa bus zala.. jaiznes lauka (lai mainitu krasu)
            }
            catch (Exception)
            {

                txtNumber1.Background = Brushes.Red;  // ja viss slitks, tad krasa sarkana
            }

            try
            {
                number2 = Convert.ToDouble(txtNumber2.Text);  // ja viss labi
               // txtNumber2.Background = Brushes.Green;              // tad kraasa bus zala
            }
            catch (Exception)
            {

                txtNumber2.Background = Brushes.Red;  // ja viss slitks, tad krasa sarkana
            }




           
            //string action = txtAction.Text;
            double result = 0;

            //switch (action)
            //{
            //    case "+":
            //        result = number1 + number2;
            //        break;
            //    case "-":
            //        result = number1 - number2;
            //        break;
            //    case "*":
            //        result = number1 * number2;
            //        break;
            //    case "/":
            //        result = number1 / number2;
            //        break;
            //    default:
            //        lblResult.Text = "Wrong result";
            //        break;
            //}

  

        }



        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            double number1 = 0;
            double number2 = 0;
            double result = 0;
            number1 = Convert.ToDouble(txtNumber1.Text);
            number2 = Convert.ToDouble(txtNumber2.Text);
            result = number1 + number2;
            lblResult.Text = Convert.ToString($"{number1} + {number2} = {result}");
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            double number1 = 0;
            double number2 = 0;
            double result = 0;
            number1 = Convert.ToDouble(txtNumber1.Text);
            number2 = Convert.ToDouble(txtNumber2.Text);
            result = number1 - number2;
            lblResult.Text = Convert.ToString($"{number1} - {number2} = {result}");

        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            double number1 = 0;
            double number2 = 0;
            double result = 0;
            number1 = Convert.ToDouble(txtNumber1.Text);
            number2 = Convert.ToDouble(txtNumber2.Text);
            result = number1 * number2;
            lblResult.Text = Convert.ToString($"{number1} * {number2} = {result}");
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            double number1 = 0;
            double number2 = 0;
            double result = 0;
            number1 = Convert.ToDouble(txtNumber1.Text);
            number2 = Convert.ToDouble(txtNumber2.Text);
            result = number1 / number2;
            lblResult.Text = Convert.ToString($"{number1} / {number2} = {result}");
        }
    }
}
