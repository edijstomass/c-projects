﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task_34
{
    class Program
    {
        static void Main(string[] args)
        {

            Chair chair1 = new Chair("Black", 42.2, 55);
            Chair chair2 = new Chair("Yellow", 48.2, 155);
            Chair chair3 = new Chair("Green", 70.2, 15);
            Chair chair4 = new Chair("Black", 10.2, 66);
            Chair chair5 = new Chair("Yellow", 140, 70);
            Chair chair6 = new Chair("Green", 36.8, 46);


            List<Chair> chairList = new List<Chair>();

            chairList.Add(chair1);
            chairList.Add(chair2);
            chairList.Add(chair3);
            chairList.Add(chair4);
            chairList.Add(chair5);
            chairList.Add(chair6);

            var onlyYellowChairs = chairList.Where(c => c.Color == "Yellow");

            foreach (var item in onlyYellowChairs)
            {
                Console.WriteLine(item);
            }



        }
    }
}
