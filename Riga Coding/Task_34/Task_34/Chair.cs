﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_34
{
   public class Chair
    {
        public Chair(string color, double weight, int lenght)
        {
            Color = color;
            Weight = weight;
            Lenght = lenght;
        }



        public string Color { get; private set; }
        public double Weight { get; private set; }
        public int Lenght { get; set; }



        public override string ToString()
        {
            return $"Color: {Color}, Weight: {Weight}, Lenght: {Lenght}";
        }


    }
}
