﻿using System;

namespace ifAndElse2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your number: ");
            string userInput = Console.ReadLine();
            int number = int.Parse(userInput);
            //int number2 = Convert.ToInt32(userInput); //piemērs var izmantot Parse vai Convert.To

            //int number = Convert.ToInt32(Console.ReadLine());   // 1. Variants
            //int number2 = int.Parse(string_name);               // 2. Variants
            //int number3 = Convert.ToInt32(string_name);         // 3. Variants


            if (number >= 0 && number <= 10 )
            {
                Console.WriteLine("A");

            }else if (number > 10)
            {
                Console.WriteLine("B");

            } else
            {
                Console.WriteLine("You failed");
            }
                

        }
    }
}
