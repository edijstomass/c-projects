﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqSamples
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int>()
            {
                1, 3, 6, 9, 1, 7, 9

            };


            List<string> names = new List<string>()
            {
                "Gusts", "Janis", "Peteris", "Andris"

            };

            var maxValue = numbers.Max();
            var minValue = numbers.Min();
            var avgValue = numbers.Average();

            foreach (var number in numbers)
            {
                if(number > 5)
                {
                
                }

            }

            var numbersGreaterThenFive = numbers.Where(number => number > 5);  //jaatceras, ka lambda ir linq

            foreach(var number in numbersGreaterThenFive)
            {
                Console.WriteLine(number);
            }



            var namesWithLetterA = names.Where(name => name.Contains("a"));

            foreach (var name in namesWithLetterA)
            {
                Console.WriteLine(name);
            }


            var person = names.FirstOrDefault(name => name == "Gusts2");

            if(person != null)
            {

            }

            Console.WriteLine(person);


            //names.Sort();

            //foreach(var name in names)
            //{
            //    Console.WriteLine(name);
            //}



            //foreach (var name in names.OrderBy(name=>name))
            //{
            //    Console.WriteLine(name);
            //}

            //foreach (var name in names.OrderByDescending(name => name))
            //{
            //    Console.WriteLine(name);
            //}

            var orderList = names.OrderByDescending(name => name);

            foreach (var name in orderList)
            {
                Console.WriteLine(name);
            }






        }
    }
}
