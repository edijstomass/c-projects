﻿using System;

namespace Array_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new[] { 1, 3, 4, 8, 0, 23 };
            string[] names = new[] { "Gusts", "Janis" };
            numbers[3] = 33;
            names[1] = "Peteris";
            try
            {
                Console.WriteLine(numbers[3]);
                Console.WriteLine(names[1]);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Element not found");
            }


        }
    }
}
