﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_37
{
    public class StudyTable : Table
    {
        public StudyTable(int weight, int height, int lenght, string color, int drawerCount)
        {
            Weight = weight;
            Height = height;
            Lenght = lenght;
            Color = color;
            DrawerCount = drawerCount;
        }


        public int DrawerCount { get; private set; }
        public string Color { get; private set; }



        public override string ToString()
        {
            return $"Study Table - Weight: {Weight}, Height: {Height}, Lenght{Lenght}, Drawers: {DrawerCount}";
        }



    }
}
