﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task_37
{
   public class KitchenTable : Table
    {

        public KitchenTable(int weight, int height, int seats, int lenght)
        {
            Weight = weight;
            Height = height;
            Seats = seats;
            Lenght = lenght;
        }




        public int Seats { get; private set; }




        public void Expend()
        {
            if (Lenght <100 && Seats == 4)
            {
                Lenght = Lenght *2;
                Seats = Seats * 2;
            }
        }


        public void Shrink()
        {
            if(Lenght > 140 && Seats == 8)
            {
                Lenght = Lenght /2;
                Seats = Seats / 2;
            }

        }


        public override string ToString()
        {
            return $"Kitchen table - Weight: {Weight}, Height: {Height}, Lenght{Lenght}, Seats: {Seats}";
        }


    }
}
