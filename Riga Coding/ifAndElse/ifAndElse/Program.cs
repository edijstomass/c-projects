﻿using System;

namespace ifAndElse
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter your name: ");
            var name = Console.ReadLine();

            var firstChar = name.Substring(0, 1);
            
            if(firstChar == "A" && name.Length > 10)
            {
                Console.WriteLine("Starts with A");

            } else if(firstChar == "B" || firstChar == "C")
            {
                Console.WriteLine("Starts with B or C");

            }
            else if ((firstChar == "E" && name.Length<9)
                                   &&(firstChar == "O"))
            {
                Console.WriteLine("Starts with B or C");

            } //else
            //{
            //    Console.WriteLine("Starts with something else");

            //}


        }
    }
}
