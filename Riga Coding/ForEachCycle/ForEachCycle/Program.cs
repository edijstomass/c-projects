﻿using System;

namespace ForEachCycle
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new[] { 1, 3, 4, 8, 0, 23 };
            string[] names = new[] { "Gusts", "Janis" };

            foreach(int number in numbers)
            {
                //Console.WriteLine($"{number}");
                if(number > 5)
                {
                    Console.WriteLine(number);
                }
            }
            foreach(string name in names)
            {
                Console.WriteLine(name);
            }

        }
    }
}
