﻿using System;

namespace Array
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array1 = new int[] {25, 50, 75, 100};
            Console.WriteLine(array1[3]);

            Console.WriteLine($"Array contains: {array1.Length} elements");


            string name = "Edijs";
            Console.WriteLine(name[0]);

            double[] array2 = new double[] { 0, 1, 50, 4, 44, 5, 99, 9 };
            string[] name5 = new string[] { "Edijs", "Zane" };


        }
    }
}
