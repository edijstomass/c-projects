﻿using System;

namespace Task_26
{
    class Program
    {
        static string name = null;
        static string surname = null;
        static void Main(string[] args)
        {
           
            GetNameSurname();
            PrintNameSurname(name, surname);

        }

        public static void GetNameSurname()  
        {
            Console.Write("Enter your name: ");
            name = Console.ReadLine();
            Console.Write("Enter your surname: ");
            surname = Console.ReadLine();
        }

        public static void PrintNameSurname(string name, string surname)
        {

            Console.WriteLine($"My name is: {name} {surname}");
            

        }



    }
}
