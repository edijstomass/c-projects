﻿using System;
using System.Collections.Generic;

namespace Task_19
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] numbers = new int[] { 4, 68, 7, 12, 56, 33, 1, 90, 4 };


            //    AR LIST

            //var my_list = new List<int>();

            //for(int i = 0; i<numbers.Length; i++)
            //{
            //    if(numbers[i] > 15)
            //    {
            //        my_list.Add(numbers[i]);
            //    }

            //}

            //foreach(int number in my_list)
            //{
            //    Console.WriteLine(number);
            //}


            //    Bez List


            int count = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > 15)
                {
                    count++;
                }
            }


            int[] numbersPlus15 = new int[count];
            int n = 0;

            for (int i = 0; i < numbers.Length; i++)
            {
                if (numbers[i] > 15)
                {
                    numbersPlus15[n] = numbers[i];
                    n++;
                }

            }

            foreach(int number in numbersPlus15)
            {
                Console.WriteLine(number);
            }


        }
    }
}
