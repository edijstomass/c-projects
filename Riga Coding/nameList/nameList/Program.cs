﻿using System;
using System.Collections.Generic;

namespace nameList
{
    class Program
    {
        const string ActionMenu = "Add name [A], Print names [P], to exit [E], to Remove [R]";
        static List<string> nameList = new List<string>();

        static void Main(string[] args)
        {

              //List<string> nameList = new List<string>(); // my_list.clear - izitra list

            Console.WriteLine(ActionMenu);


            
            while (true)
            {



               var action = Console.ReadLine();

                switch (action.ToUpper())
                {
                    case "R":
                        RemoveName();
                        break;
                    case "E":
                        return; //izbeidz metodi (programmu)
                    case "A":
                        AddName();
                        break;
                    case "P":
                        PrintName();
                        break;
                    default:
                        Console.WriteLine($"Please enter valid action: {ActionMenu}");
                        break;
                        
                }



            }







        }

        static void AddName()
        {
            //create add to list logic
            Console.Write("Enter name: ");
            var enteredName = Console.ReadLine();
            if (!string.IsNullOrEmpty(enteredName))
            {
                nameList.Add(enteredName);
            }
        }

        static void PrintName()
        {
            //add print list logic
            Console.WriteLine("Names in the list");
            foreach (var name in nameList)
            {
                Console.WriteLine(name);
            }
        }

        static void RemoveName()
        {
            Console.WriteLine("Enter name, to remove it");
            nameList.Remove(Console.ReadLine());
        }


    }
}
