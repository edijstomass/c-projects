﻿using System;

namespace Task_findMinValue
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[] { 2, 5, 7, 9, 3, 6, 0 };
            int minValue = int.MaxValue;

            for (int i = 0; i < numbers.Length; i++)
            {


                if (numbers[i] < minValue)
                {
                    minValue = numbers[i];


                }

            }

            Console.WriteLine($"Min Value is : {minValue}");
        }
    }
}
