﻿using System;
using PATA = ProjectA.TeamA;
using PATB = ProjectA.TeamB;

namespace nameSpaceExample
{
    class Program
    {
        static void Main(string[] args)
        {
            ProjectA.TeamA.ClassA.Print();
            ProjectA.TeamB.ClassA.Print();

            PATA.ClassA.Print();   // tas pats, tikai using namespace alias
            PATB.ClassA.Print();

        }
    }
}
